# Sign-up Service

## 1-Add Test

```java
@ExtendWith(MockitoExtension.class)
@Provider("Sign up Service")
@Consumer("Location Promotion Service")
@PactBroker(host = "${pactbroker.host:localhost}", port = "${pactbroker.port:8082}")
public class LocationPromotionContractTest {

    @BeforeEach
    void setup(PactVerificationContext context) {
        context.setTarget(new HttpTestTarget("localhost", 8888));
    }

    @TestTemplate
    @ExtendWith(PactVerificationInvocationContextProvider.class)
    void pactVerificationTestTemplate(PactVerificationContext context) {
        context.verifyInteraction();
    }

}
```

## 2- setup Application in Test

```java
    private static final BarRepository BAR_REPOSITORY = null;

    @Mock
    private UserRepository userRepository;
    private SignUp signUp;

    @BeforeEach
    void setup(PactVerificationContext context) {
        context.setTarget(new HttpTestTarget("localhost", 8888));

        signUp = new SignUp(8888, userRepository, BAR_REPOSITORY);
        signUp.start();
        signUp.awaitInitialization();
    }

    @AfterEach
    public void tearDown() {
        signUp.stop();
    }

    @TestTemplate
    @ExtendWith(PactVerificationInvocationContextProvider.class)
    void pactVerificationTestTemplate(PactVerificationContext context) {
        context.verifyInteraction();
    }
```

## 3- Mock States

```java
    @State("has no users in France")
    public void hasNoUserInFrance() {
        when(userRepository.findByLocation(eq("France"))).thenReturn(empty());
    }

    @State("has users in Belgium")
    public void hasUsersInBelgium() {
        User matteo = aUser().withFirstName("Matteo").withLastName("Pierro").withEmail("foo@bar.com").build();
        User nelis = aUser().withFirstName("Nelis").withLastName("Boucke").withEmail("fizz@baz.be").build();
        when(userRepository.findByLocation(eq("Belgium"))).thenReturn(Stream.of(matteo, nelis));
    }
```

## 4- Implement create() in UserRouter

```java
    @Override
    public void create() {
        get("/api/users", this::findUsers);
    }

    private String findUsers(Request request, Response response) {
        return "";
    }
```

## 5- Implement findUsers in UserRouter

```java
    private String findUsers(Request request, Response response) {
        String location = request.queryParams("location");
        Stream<User> users = userRepository.findByLocation(location);

        response.type("application/json");
        return toJsonString(users);
    }

    private String toJsonString(Stream<User> users) {
        JsonArray jsonUsers = new JsonArray();
        users.forEach(user -> jsonUsers.add(toJson(user)));
        return jsonUsers.toString();
    }

    private JsonObject toJson(User user) {
        JsonObject jsonUser = new JsonObject();
        jsonUser.add("first_name", user.firstName());
        jsonUser.add("last_name", user.lastName());
        jsonUser.add("email", user.email());
        return jsonUser;
    }
```