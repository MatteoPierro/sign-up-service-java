FROM openjdk:11

COPY target/sign-up-service-fat.jar sign-up-service.jar

CMD ["java", "-jar", "sign-up-service.jar"]