package com.wesquad.signup;

import au.com.dius.pact.provider.junit.Consumer;
import au.com.dius.pact.provider.junit.Provider;
import au.com.dius.pact.provider.junit.State;
import au.com.dius.pact.provider.junit.loader.PactBroker;
import au.com.dius.pact.provider.junit5.HttpTestTarget;
import au.com.dius.pact.provider.junit5.PactVerificationContext;
import au.com.dius.pact.provider.junit5.PactVerificationInvocationContextProvider;
import com.wesquad.signup.domain.model.bar.Bar;
import com.wesquad.signup.domain.model.bar.BarRepository;
import com.wesquad.signup.domain.model.user.UserRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.TestTemplate;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static java.util.Optional.empty;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@Provider("Sign up Service")
@Consumer("Bar proxy Service")
@PactBroker(host = "${pactbroker.host:localhost}", port = "${pactbroker.port:8082}")
public class BarProxyContractTest {

    private final UserRepository USER_REPOSITORY = null;

    @Mock
    private BarRepository barRepository;
    private SignUp signUp;

    @BeforeEach
    void setup(PactVerificationContext context) {
        context.setTarget(new HttpTestTarget("localhost", 7777));

        signUp = new SignUp(7777, USER_REPOSITORY, barRepository);
        signUp.start();
        signUp.awaitInitialization();
    }

    @AfterEach
    void tearDown() {
        signUp.stop();
    }

    @TestTemplate
    @ExtendWith(PactVerificationInvocationContextProvider.class)
    void pactVerificationTestTemplate(PactVerificationContext context) {
        context.verifyInteraction();
    }

    @State("has no bar")
    public void hasNotBar() {
        when(barRepository.findOne()).thenReturn(empty());
    }

    @State("has bar")
    public void hasBar() {
        Bar bar = new Bar("123-456", "foo");
        when(barRepository.findOne()).thenReturn(Optional.of(bar));
    }
}