package com.wesquad.signup.domain.model.user;

import java.util.Date;
import java.util.UUID;

public class UserBuilder {
    private String id = UUID.randomUUID().toString();
    private String firstName = "first name";
    private String lastName = "last name";
    private String email = "foo@bar.com";
    private String password = "password";
    private String location = "Brasil";
    private Date birthday = new Date();

    public static UserBuilder aUser() {
        return new UserBuilder();
    }

    private UserBuilder() {

    }

    public UserBuilder withId(String id) {
        this.id = id;
        return this;
    }

    public UserBuilder withFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public UserBuilder withLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public UserBuilder withEmail(String email) {
        this.email = email;
        return this;
    }

    public UserBuilder withPassword(String password) {
        this.password = password;
        return this;
    }

    public UserBuilder withLocation(String location) {
        this.location = location;
        return this;
    }

    public UserBuilder withBirthday(Date birthday) {
        this.birthday = birthday;
        return this;
    }

    public User build() {
        return new User(id, firstName, lastName, email, password, location, birthday);
    }
}