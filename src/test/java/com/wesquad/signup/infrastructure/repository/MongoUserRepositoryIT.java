package com.wesquad.signup.infrastructure.repository;

import com.wesquad.signup.domain.model.user.User;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


import java.util.List;

import static com.wesquad.signup.domain.model.user.UserBuilder.aUser;
import static java.util.stream.Collectors.toList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class MongoUserRepositoryIT {

    private static final String MONGO_ENDPOINT = "mongodb://localhost";
    private MongoUserRepository repository;

    @BeforeEach
    public void setUp() throws Exception {
        repository = new MongoUserRepository(MONGO_ENDPOINT);
    }

    @AfterEach
    public void tearDown() throws Exception {
        repository.drop();
    }

    @Test
    public void shouldRetrieveUserInBelgium() throws Exception {
        User belgianUser = aUser().withLocation("Belgium").build();
        repository.save(belgianUser);

        List<User> belgianUsers = repository.findByLocation("Belgium").collect(toList());

        assertFalse(belgianUsers.isEmpty());
        assertEquals(belgianUser, belgianUsers.get(0));
    }
}