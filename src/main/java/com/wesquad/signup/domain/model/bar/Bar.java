package com.wesquad.signup.domain.model.bar;

public class Bar {

    private final String id;
    private final String value;

    public Bar(String id, String value) {
        this.id = id;
        this.value = value;
    }

    public String id() {
        return id;
    }

    public String value() {
        return value;
    }
}
