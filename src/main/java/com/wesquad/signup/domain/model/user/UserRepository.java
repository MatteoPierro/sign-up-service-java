package com.wesquad.signup.domain.model.user;

import java.util.stream.Stream;

public interface UserRepository {

    void save(User user);
    Stream<User> findByLocation(String location);
}
