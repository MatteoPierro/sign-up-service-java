package com.wesquad.signup.domain.model.bar;

import java.util.Optional;

public interface BarRepository {

    void save(Bar bar);

    Optional<Bar> findOne();

    long count();
}
