package com.wesquad.signup.domain;

import java.util.UUID;

public class IdGenerator {

    public String next() {
        return UUID.randomUUID().toString();
    }
}
