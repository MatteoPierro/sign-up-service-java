package com.wesquad.signup;

import com.wesquad.signup.domain.model.bar.BarRepository;
import com.wesquad.signup.infrastructure.repository.MongoBarRepository;
import com.wesquad.signup.infrastructure.repository.MongoUserRepository;
import com.wesquad.signup.domain.model.user.UserRepository;

public class SignUpLauncher {
    public static void main(String[] args) throws Exception {
        UserRepository userRepository = new MongoUserRepository(mongoUrl());
        BarRepository barRepository = new MongoBarRepository(mongoUrl());
        SignUp signUp = new SignUp(port(), userRepository, barRepository);
        signUp.start();
    }

    private static int port() {
        String port = System.getenv("SIGN_UP_PORT");
        return port != null
                ? Integer.parseInt(port)
                : 4567;
    }

    private static String mongoUrl() {
        return "mongodb://" + mongoHost();
    }

    private static String mongoHost() {
        String mongoHost = System.getenv("MONGO_HOST");
        return mongoHost != null
                ? mongoHost
                : "localhost";
    }
}
