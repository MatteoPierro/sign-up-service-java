package com.wesquad.signup;

import com.wesquad.signup.domain.model.bar.BarRepository;
import com.wesquad.signup.domain.model.user.UserRepository;
import com.wesquad.signup.infrastructure.router.api.APIRouter;
import com.wesquad.signup.infrastructure.router.web.WebRouter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.Spark;

import static spark.Spark.*;

public class SignUp {
    private static Logger logger = LoggerFactory.getLogger(SignUp.class);

    private final int port;
    private final WebRouter webRouter;
    private final APIRouter apiRouter;

    public SignUp(int port, UserRepository userRepository, BarRepository barRepository) {
        this.port = port;
        this.webRouter = new WebRouter(userRepository);
        this.apiRouter = new APIRouter(barRepository, userRepository);
    }

    public void start() {
        port(port);
        staticFiles.location("/public");
        setLog();
        webRouter.create();
        apiRouter.create();
    }

    public void stop() {
        Spark.stop();
        Spark.awaitStop();
    }

    public void awaitInitialization() {
        Spark.awaitInitialization();
    }

    private void setLog() {
        before((request, response) -> {
            logger.info("URL request: " + request.requestMethod() + " " + request.uri() + " - headers: " + request.headers());
        });
    }
}
