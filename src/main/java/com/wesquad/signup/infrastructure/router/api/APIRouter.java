package com.wesquad.signup.infrastructure.router.api;

import com.wesquad.signup.domain.model.bar.BarRepository;
import com.wesquad.signup.domain.model.user.UserRepository;
import com.wesquad.signup.infrastructure.router.Router;

public class APIRouter implements Router {

    private final BarRouter barRouter;
    private final UserRouter userRouter;

    public APIRouter(BarRepository barRepository, UserRepository userRepository) {
        barRouter = new BarRouter(barRepository);
        userRouter = new UserRouter(userRepository);
    }

    @Override
    public void create() {
        barRouter.create();
        userRouter.create();
    }
}
