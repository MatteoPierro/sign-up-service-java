package com.wesquad.signup.infrastructure.router.web;

import com.wesquad.signup.domain.model.user.UserRepository;
import com.wesquad.signup.infrastructure.router.Router;

import static spark.Spark.get;

public class WebRouter implements Router {

    private final UserRouter userRouter;

    public WebRouter(UserRepository userRepository) {
        userRouter = new UserRouter(userRepository);
    }

    @Override
    public void create() {
        userRouter.create();
        get("/hello", (req, res) -> "Hello!");
    }
}
