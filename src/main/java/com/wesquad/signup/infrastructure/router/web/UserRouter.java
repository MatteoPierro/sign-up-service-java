package com.wesquad.signup.infrastructure.router.web;

import com.wesquad.signup.domain.IdGenerator;
import com.wesquad.signup.infrastructure.controller.web.UserController;
import com.wesquad.signup.domain.model.user.UserRepository;
import com.wesquad.signup.infrastructure.router.Router;

import static spark.Spark.post;

public class UserRouter implements Router {
    private final UserController userController;

    public UserRouter(UserRepository userRepository) {
        this.userController = new UserController(userRepository, new IdGenerator());
    }

    @Override
    public void create() {
        post("/users", userController::signUp);
    }
}
