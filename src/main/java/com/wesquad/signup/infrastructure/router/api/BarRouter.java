package com.wesquad.signup.infrastructure.router.api;

import com.wesquad.signup.domain.IdGenerator;
import com.wesquad.signup.domain.model.bar.BarRepository;
import com.wesquad.signup.infrastructure.controller.api.BarController;
import com.wesquad.signup.infrastructure.router.Router;

import static spark.Spark.post;
import static spark.Spark.get;

public class BarRouter implements Router {

    private final BarController controller;

    public BarRouter(BarRepository repository) {
        controller = new BarController(repository, new IdGenerator());
    }

    @Override
    public void create() {
        post("/api/bar", controller::createBar);
        get("/api/bar", controller::findOne);
        get("/api/bar/count", controller::countAll);
    }
}
