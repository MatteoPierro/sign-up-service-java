package com.wesquad.signup.infrastructure.router.api;

import com.wesquad.signup.domain.model.user.UserRepository;
import com.wesquad.signup.infrastructure.router.Router;

public class UserRouter implements Router {

    private final UserRepository userRepository;

    public UserRouter(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public void create() {

    }
}
