package com.wesquad.signup.infrastructure.repository;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.wesquad.signup.domain.model.bar.Bar;
import com.wesquad.signup.domain.model.bar.BarRepository;

import java.util.Optional;

import static java.util.Optional.empty;

public class MongoBarRepository implements BarRepository {

    private final MongoRepository repository;

    public MongoBarRepository(String url) throws Exception {
        repository = new MongoRepository(url, "bars");
    }

    @Override
    public void save(Bar bar) {
        repository.save(barToDBObject(bar));
    }

    @Override
    public Optional<Bar> findOne() {
        return dbObjectToBar(repository.findOne());
    }

    @Override
    public long count() {
        return repository.countDocuments();
    }

    private DBObject barToDBObject(Bar bar) {
        return new BasicDBObject()
                .append("_id", bar.id())
                .append("value", bar.value());
    }

    private Optional<Bar> dbObjectToBar(DBObject dbObject) {
        if (dbObject == null) return empty();

        String id = (String) dbObject.get("_id");
        String value = (String) dbObject.get("value");
        return Optional.of(new Bar(id, value));
    }
}
