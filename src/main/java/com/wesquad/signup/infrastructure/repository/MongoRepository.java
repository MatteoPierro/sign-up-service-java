package com.wesquad.signup.infrastructure.repository;

import com.mongodb.*;

import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class MongoRepository {

    private final DBCollection collection;

    public MongoRepository(String url, String collectionName) throws Exception {
        MongoClient mongoClient = new MongoClient(new MongoClientURI(url));
        DB db = mongoClient.getDB("signup_db");
        collection = collection(db, collectionName);
    }

    public void save(DBObject dbObject) {
        collection.insert(dbObject);
    }

    public DBObject findOne() {
        return collection.findOne();
    }

    public Stream<DBObject> find(DBObject query) {
        DBCursor queryResult = collection.find(query);
        return StreamSupport
                .stream(queryResult.spliterator(), false);
    }

    public long countDocuments() {
        return collection.count();
    }

    public void drop() {
        collection.drop();
    }

    private DBCollection collection(DB db, String collectionName) {
        if (db.collectionExists(collectionName))
            return db.getCollection(collectionName);
        return db.createCollection(collectionName, null);
    }
}
