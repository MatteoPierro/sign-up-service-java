package com.wesquad.signup.infrastructure.repository;

import com.mongodb.BasicDBObject;
import com.mongodb.BasicDBObjectBuilder;
import com.mongodb.DBObject;
import com.wesquad.signup.domain.model.user.User;
import com.wesquad.signup.domain.model.user.UserRepository;

import java.util.Date;
import java.util.stream.Stream;

public class MongoUserRepository implements UserRepository {

    private final MongoRepository repository;

    public MongoUserRepository(String url) throws Exception {
        repository = new MongoRepository(url, "users");
    }

    @Override
    public void save(User user) {
        repository.save(userToDBObject(user));
    }

    @Override
    public Stream<User> findByLocation(String location) {
        DBObject query = BasicDBObjectBuilder.start().add("location", location).get();
        return repository.find(query)
                .map(MongoUserRepository::dbObjectToUser);
    }

    void drop() {
        repository.drop();
    }

    private static User dbObjectToUser(DBObject dbObject) {
        String id = getString(dbObject, "_id");
        String firstName = getString(dbObject, "first_name");
        String lastName  = getString(dbObject, "last_name");
        String email = getString(dbObject, "email");
        String password = getString(dbObject, "password");
        String location = getString(dbObject, "location");
        Date birthday = (Date) dbObject.get("birthday");
        return new User(id, firstName, lastName, email, password, location, birthday);
    }

    private static String getString(DBObject dbObject, String key) {
        return (String) dbObject.get(key);
    }

    private static DBObject userToDBObject(User user) {
        return new BasicDBObject()
                                .append("_id", user.id())
                                .append("first_name", user.firstName())
                                .append("last_name", user.lastName())
                                .append("email", user.email())
                                .append("location", user.location())
                                .append("birthday", user.birthday())
                                .append("password", user.password());
    }
}
