package com.wesquad.signup.infrastructure.controller.api;

import com.eclipsesource.json.Json;
import com.eclipsesource.json.JsonObject;
import com.wesquad.signup.domain.IdGenerator;
import com.wesquad.signup.domain.model.bar.Bar;
import com.wesquad.signup.domain.model.bar.BarRepository;
import spark.Request;
import spark.Response;

import java.util.Optional;

import static org.eclipse.jetty.http.HttpStatus.*;
import static org.eclipse.jetty.http.MimeTypes.Type.APPLICATION_JSON;
import static org.eclipse.jetty.http.MimeTypes.Type.TEXT_PLAIN;

public class BarController {

    private final BarRepository repository;
    private final IdGenerator idGenerator;

    public BarController(BarRepository repository, IdGenerator idGenerator) {
        this.repository = repository;
        this.idGenerator = idGenerator;
    }

    public String createBar(Request request, Response response) {
        String id = idGenerator.next();
        String value = valueFrom(request);
        Bar bar = new Bar(id, value);

        repository.save(bar);

        response.status(CREATED_201);
        response.type(APPLICATION_JSON.asString());
        return toJson(bar);
    }

    public String findOne(Request request, Response response) {
        Optional<Bar> aBar = repository.findOne();
        if (aBar.isEmpty()) {
            response.status(NOT_FOUND_404);
            return "";
        }

        response.status(OK_200);
        response.type(APPLICATION_JSON.asString());
        return toJson(aBar.get());
    }

    public String countAll(Request request, Response response) {
        response.status(OK_200);
        response.type(TEXT_PLAIN.asString());
        return String.valueOf(repository.count());
    }

    private String toJson(Bar bar) {
        return new JsonObject()
                            .add("id", bar.id())
                            .add("value", bar.value())
                            .toString();
    }

    private String valueFrom(Request request) {
        JsonObject jsonBody = Json.parse(request.body()).asObject();
        return jsonBody.get("value").asString();
    }
}
