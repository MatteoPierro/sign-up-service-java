package com.wesquad.signup.infrastructure.controller.web;

import com.wesquad.signup.domain.IdGenerator;
import com.wesquad.signup.domain.model.user.User;
import com.wesquad.signup.domain.model.user.UserRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.template.jade.JadeTemplateEngine;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

public class UserController {

    private static final SimpleDateFormat BIRTHDAY_FORMATTER = new SimpleDateFormat("yyyy-MM-dd");
    private final UserRepository repository;
    private final IdGenerator idGenerator;

    public UserController(UserRepository repository, IdGenerator idGenerator) {
        this.repository = repository;
        this.idGenerator = idGenerator;
    }

    public String signUp(Request request, Response response) {
        try {
            User user = userFrom(request);
            repository.save(user);
            Map<String, String> model = Map.of("first_name", user.firstName());
            ModelAndView signupSuccess = new ModelAndView(model, "signup-success");
            return new JadeTemplateEngine().render(signupSuccess);
        } catch (Exception e) {
            e.printStackTrace();
            response.status(500);
            return "";
        }
    }

    private User userFrom(Request request) throws Exception {
        String id = idGenerator.next();
        String firstName = request.queryParams("first_name");
        String lastName = request.queryParams("last_name");
        String email = request.queryParams("email");
        String password = request.queryParams("password");
        String location = request.queryParams("location");
        String birthdayString = request.queryParams("birthday");
        Date birthday = BIRTHDAY_FORMATTER.parse(birthdayString);
        return new User(id, firstName, lastName, email, password, location, birthday);
    }
}
